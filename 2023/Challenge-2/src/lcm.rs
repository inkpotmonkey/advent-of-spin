// use num_traits::PrimInt;

// fn lcm_list<T>(vec: Vec<T>) -> T
// where
//     T: PrimInt,
// {
//     vec.into_iter().fold(T::one(), |acc, num| lcm(acc, num))
// }

// pub fn lcm<T>(first: T, second: T) -> T
// where
//     T: PrimInt,
// {
//     first * second / gcd(first, second)
// }

// fn gcd<T>(mut first: T, mut second: T) -> T
// where
//     T: PrimInt,
// {
//     while second != T::zero() {
//         let temp = second;
//         second = first % second;
//         first = temp;
//     }
//     first
// }
