use http::StatusCode;
use serde::{Deserialize, Serialize};
use spin_sdk::{
    http::{IntoResponse, Method, Request, Response},
    http_component,
};
use std::cmp::max;

#[derive(Deserialize)]
struct RequestBody {
    kids: Vec<usize>,
    weight: Vec<usize>,
    capacity: usize,
}

#[derive(Serialize)]
struct ResponseBody {
    kids: usize,
}

/// A simple Spin HTTP component.
#[http_component]
fn handle_tmp(req: Request) -> anyhow::Result<impl IntoResponse> {
    if *req.method() == Method::Post {
        let req_body =
            String::from_utf8(req.body().to_vec()).expect("Failed to convert body to string");

        let req_body: RequestBody = serde_json::from_str(&req_body).expect("Failed to parse JSON");

        let max_kids = knapsack(req_body.kids, req_body.weight, req_body.capacity);

        let res_body = ResponseBody { kids: max_kids };

        let res_body = serde_json::to_string(&res_body)?;

        return Ok(Response::builder()
            .status(200)
            .header("content-type", "application/json")
            .body(res_body)
            .build());
    }

    Ok(Response::new(StatusCode::BAD_REQUEST, None::<String>))
}

fn knapsack(kids: Vec<usize>, weight: Vec<usize>, capacity: usize) -> usize {
    let width = (1 + capacity).try_into().unwrap();
    let height = kids.len() + 1;
    let mut solution_space = vec![vec![0; width]; height];

    for row_index in 1..height {
        for col_index in 1..width {
            if weight[row_index - 1] <= col_index {
                let value_one = solution_space[row_index - 1][col_index];
                let value_two = kids[row_index - 1]
                    + solution_space[row_index - 1][col_index - weight[row_index - 1]];

                solution_space[row_index][col_index] = max(value_one, value_two);
            } else {
                solution_space[row_index][col_index] = solution_space[row_index - 1][col_index]
            }
        }
    }

    solution_space[height - 1][width - 1]
}
