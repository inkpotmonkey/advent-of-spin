use http::{Method, StatusCode};
use serde::{Deserialize, Serialize};
use spin_sdk::{
    http::{IntoResponse, Response},
    http_component,
    key_value::Store,
};

#[derive(Serialize)]
struct JsonResponse {
    value: String,
}

#[derive(Deserialize)]
struct JsonRequest {
    value: String,
}

#[http_component]
fn handle_request(req: http::Request<Vec<u8>>) -> anyhow::Result<impl IntoResponse> {
    match req.uri().query() {
        None => {
            return Ok(Response::new(
                StatusCode::BAD_REQUEST,
                "API expects a query string that maps to the value being accessed in the store",
            ));
        }
        Some(query) => {
            let store = Store::open_default()?;

            let (status, body, content_type) = match *req.method() {
                Method::POST => {
                    let body_str = String::from_utf8(req.body().to_vec())
                        .expect("Failed to convert body to string");

                    // Parse the string as JSON
                    let parsed: JsonRequest =
                        serde_json::from_str(&body_str).expect("Failed to parse JSON");

                    // Extract the value associated with the key "value"
                    if &parsed.value == "value" {
                        // Your logic for using the value
                        println!("Extracted value: {}", &parsed.value);

                        store.set(query, &parsed.value.into_bytes())?;

                        (StatusCode::CREATED, "Success".to_string(), "text/html")
                    } else {
                        (
                            StatusCode::BAD_REQUEST,
                            "API expects body to be in the format: {\"value\": \"data\"}"
                                .to_string(),
                            "text/html",
                        )
                    }
                }

                Method::GET => match store.get(query)? {
                    Some(value) => {
                        let string_value =
                            String::from_utf8(value).expect("Failed to convert Vec<u8> to String");

                        let json_response = JsonResponse {
                            value: string_value,
                        };

                        let json_body = serde_json::to_string(&json_response)?;

                        (StatusCode::OK, json_body, "application/json")
                    }
                    None => (
                        StatusCode::NOT_FOUND,
                        format!("No value found for the key {:?}", query),
                        "text/html",
                    ),
                },

                Method::DELETE => {
                    if store.exists(query)? {
                        // Delete the value associated with the request URI, if present
                        store.delete(query)?;
                        (
                            StatusCode::OK,
                            format!("Deleted key {:?}", query),
                            "text/html",
                        )
                    } else {
                        (
                            StatusCode::NOT_FOUND,
                            format!("{:?} key not found", query),
                            "text/html",
                        )
                    }
                }

                Method::HEAD => {
                    if store.exists(query)? {
                        (
                            StatusCode::OK,
                            format!("{:?} key found", query),
                            "text/html",
                        )
                    } else {
                        (
                            StatusCode::NOT_FOUND,
                            format!("{:?} key not found", query),
                            "text/html",
                        )
                    }
                }

                _ => (StatusCode::METHOD_NOT_ALLOWED, "".to_string(), "text/html"),
            };

            Ok(Response::builder()
                .status(status)
                .header("Content-Type", content_type)
                .body(body)
                .build())
        }
    }
}
