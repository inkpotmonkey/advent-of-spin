use anyhow::Result;
use serde::Serialize;
use serde_json;
use spin_sdk::{
    http::{Request, Response},
    http_component,
};

#[derive(Serialize)]
struct JsonResponse {
    message: String,
}

/// A simple Spin HTTP component.
#[http_component]
fn handle_advent_of_spin_2022_1(req: Request) -> Result<Response> {
    println!("{:?}", req.headers());

    let json_response = JsonResponse {
        message: "Hello, world!".to_string(),
    };

    let json_body = serde_json::to_string(&json_response)?;

    Ok(http::Response::builder()
        .status(200)
        .header("Content-Type", "application/json")
        .body(Some(json_body.into()))?)
}
