use anyhow::Result;
use http::{Request, StatusCode};
use serde::Serialize;
use serde_json::Value;
use spin_sdk::{
    http::{IntoResponse, Params, Response, Router},
    http_component,
};
use std::collections::HashMap;

#[derive(Serialize)]
struct JsonResponse {
    message: String,
}

/// A Spin HTTP component that internally routes requests.
#[http_component]
fn handle_route(req: Request<Vec<u8>>) -> Result<impl IntoResponse> {
    let mut router = Router::new();

    router.get("/", |_req: Request<Vec<u8>>, _params: Params| {
        Response::builder().status(StatusCode::NOT_FOUND).build()
    });

    router.post("/lowercase", lowercase);

    router.get("/hello", hello);
    router.get("/hello/:name", hello);

    Ok(router.handle(req))
}

pub fn hello(_req: Request<Vec<u8>>, params: Params) -> Response {
    // handle route parameters
    let name = match params.get("name") {
        Some(value) => value,
        None => "world",
    }
    .to_lowercase();

    let json_response = JsonResponse {
        message: format!("Hello, {name}!"),
    };

    let json_body = match serde_json::to_string(&json_response) {
        Ok(body) => body,
        Err(_) => {
            return Response::builder()
                .status(StatusCode::INTERNAL_SERVER_ERROR)
                .build()
        }
    };

    Response::builder()
        .status(StatusCode::OK)
        .header("Content-Type", "application/json")
        .body(Some(json_body))
        .build()
}

pub fn lowercase(req: Request<Vec<u8>>, _params: Params) -> Response {
    let body_str =
        String::from_utf8(req.body().to_vec()).expect("Failed to convert body to string");

    // Parse the string as JSON
    let parsed: HashMap<String, Value> =
        serde_json::from_str(&body_str).expect("Failed to parse JSON");

    if let Some(Value::String(value)) = parsed.get("value") {
        let json_response = JsonResponse {
            message: value.to_lowercase().to_string(),
        };

        let json_body = match serde_json::to_string(&json_response) {
            Ok(body) => body,
            Err(_) => {
                return Response::builder()
                    .status(StatusCode::INTERNAL_SERVER_ERROR)
                    .build();
            }
        };

        Response::builder()
            .status(StatusCode::OK)
            .header("Content-Type", "application/json")
            .body(Some(json_body))
            .build()
    } else {
        Response::builder().status(StatusCode::BAD_REQUEST).build()
    }
}
