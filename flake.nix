{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    rust-overlay.url = "github:oxalica/rust-overlay";
  };

  outputs = { self, nixpkgs, rust-overlay, }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
        overlays = [ rust-overlay.overlays.default ];
      };
      toolchain = pkgs.rust-bin.fromRustupToolchainFile ./toolchain.toml;
      spin-version = "2.0.1";
      packageHash = "sha256-mZwTbSbKGtQa7NJcOlBpALdr3yzv0gr8ZfckGwb4jvc=";
      platform = "linux-amd64";
      spin = pkgs.fermyon-spin.overrideAttrs {
        version = spin-version;
        src = pkgs.fetchzip {
          url =
            "https://github.com/fermyon/spin/releases/download/v${spin-version}/spin-v${spin-version}-${platform}.tar.gz";
          stripRoot = false;
          sha256 = packageHash;
        };
      };
    in {
      devShells.${system}.default = pkgs.mkShell {
        packages = [
          (builtins.attrValues { inherit (pkgs) hurl gnumake; })
          spin
          toolchain
        ];
      };
    };
}
